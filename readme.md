# indigo-fiber-navigation

Fiber Navigation is a stacked navigation system for Fiber. It allows you to seamlessly implement routing in serverless single-page apps. Currently the main features are route caching, and optional animation. We aim to provide built-in support for animation in the near future. We also aim to add routing specifically designed for modals on mobile devices, as this is far more common than page-by-page navigation. 
