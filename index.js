import routes from './routes.js'


const cache = {}
window.cache = cache

function validRoute(route) {
  return routes.hasOwnProperty(route)
}


function renderRoute(route) {
  const component = cache[route] ?? routes[route]()

  cache[route] ?? (cache[route] = component)
  return component()
}


function changeRoute(route, top) {
  if (! validRoute(route))
    return;

  // TODO: animate this
  return insert(top,
    renderRoute(route)
  )
}


export default function(route='/') {

  return {
    route,
    Outlet() {

      const top = document.createElement('div')
      route.on(route => changeRoute(route, top))

      return top
    }
  }
}
